# Chat glue mod for Minetest

This mod "glues" other chat mods together so they don't conflict with each other.

# How it works

When a mod uses the `minetest.register_on_chat_message` and returns `true`, then the `minetest.register_on_chat_message` function will not be called for any other mods. This works fine if you only have one mod that does this. But if you have multiple mods that modify the chat message this is a problem. What this mod does is load before any other chat mods, check if you have any chat mods enabled, then apply those mods's effects before returning `true`, so that `minetest.register_on_chat_message` will not be called for any other mod.

For this to work, a chat mod must have `chat_glue` as an `optional_depends`. And the mod must have an API that can be accessed by other mods.
