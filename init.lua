-- Copyright (c) 2024 Olivia May
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- Simple mod that "glues" mods that modify the chat together.

local has_chat_anticurse = minetest.get_modpath("chat_anticurse")
local has_ranks = minetest.get_modpath("ranks")
local has_pronouns = minetest.get_modpath("pronouns")
local has_dcwebhook = minetest.get_modpath("dcwebhook")
--TODO: chat3

-- Copied from ranks mod
-- [local function] Get colour
local function get_colour(colour)
	if type(colour) == "table" and minetest.rgba then
		return minetest.rgba(colour.r, colour.g, colour.b, colour.a)
	elseif type(colour) == "string" then
		return colour
	else
		return "#ffffff"
	end
end

minetest.register_on_chat_message(function(name, message)

	if has_chat_anticurse then
		if chat_anticurse.is_curse_found(name, message) then
			return true
		end
	end

	local ranks_prefix = ""
	local ranks_uncolored_prefix = ""
	if has_ranks then
		local rank = ranks.get_rank(name)

		if rank then
			local def = ranks.get_def(rank)
			if def.prefix then

				local colour = get_colour(def.colour)
				ranks_uncolored_prefix = def.prefix
				local prefix = minetest.colorize(colour, def.prefix)
				if def.babybox then
					message = ranks.babybox(message)
				end

				ranks_uncolored_prefix = ranks_uncolored_prefix .. " "
				ranks_prefix = prefix .. " "
			end
		end
	end

	local pronouns_str = ""
	if has_pronouns then
		pronouns_str = pronouns.get_str(name)
		pronouns_str = pronouns_str
	end

	minetest.chat_send_all(
		ranks_prefix .. "<" .. name .. "> " .. pronouns_str .. message)

	if has_dcwebhook then
		dcwebhook.send_webhook({
			content = ranks_uncolored_prefix .. dcwebhook.wrap_name(name) .. " " .. pronouns_str .. "  " .. message
		})
	end

	return true
end)
